#trial for getting stock data

#takes a while to run so be patient
import csv
import os

price_list = []
temp_list = []
date_list = []
#opens file with list of S&P stocks
with open ("trial.txt", "r") as myfile:
    data=myfile.readlines()
#opens the csv file for each stock and reads closing price for each day into
#variable new_list which is then appended to price_list which is sorted by stock
    for row in data:
        if os.path.exists(str(row).replace("\n","") + ".csv"):           
           with open(str(row).replace("\n","") + ".csv", 'r') as csvfile:
               info = csv.reader(csvfile)
               new_list = []
               j=0
               for row in info:
                   new_list.append([float(row[5])])
                   j = j+1
        price_list.append(new_list)

#make a new list called date_list which sorts prices of each stock for certain dates
for j in range(0,251):
    for i in range(0,49):
        temp_list.append(price_list[i][j])
    date_list.append(temp_list)
    temp_list = []
print(date_list[0])
print(date_list[1])


