# Genetic Algorithm for Optimizing Portfolios of Stocks

# import modules
from numpy import *
import random
import ystockquote
import urllib

# Import stock data through Yahoo Finance API
sp_list = [line.rstrip() for line in urllib.urlopen("https://dl.dropboxusercontent.com/u/18506382/sp100_list.txt")] #sp50,100,500

stock_list = []
for dta in sp_list :
  data = ystockquote.get_historical_prices(dta,"20130301", "20130428")
  t = map ((lambda x:float(x[6])), data[1:])
  stock_list.append(t)

sp_list = []

for i in range(len(stock_list[0])) :
    t = map ((lambda x:x[i]), stock_list)
    sp_list.append(t)

# reverse the time order of the list, now the latest price comes from the last in the list
sp_list.reverse()

# Evaluate function to calculate score for each portfolio

# define the risk tolarance
risk = 0.03

def evaluate(p,t,A):
  # get the lens of A
  n_of_d = len(A)
  n_of_stock = len(A[0])
  # get the value of the portfolio at time i 
  v = []
  for i in range(2*t):
    x = 0
    for j in range(n_of_stock):
      x = x + (p[j] * A[n_of_d-2*t+i][j])
    v.append(x)
  r = []
  for i in range(t):
    r.append(v[i+t]/v[i])
  r_sum = 0
  for i in range(t):
    r_sum = r_sum + r[i]
  r_mean = r_sum / t
  s=0
  for i in range(t):
    s = s + (r[i]-r_mean)*(r[i]-r_mean)
  r_sd = math.sqrt(s/t)
  print r_sd
  if r_sd > risk:
    return r_mean-r_sd+risk
  else:
    return r_mean

A = arange(800).reshape(8,100)
p1 = A[1, :]
print p1
print evaluate(p1,20,sp_list) 


# compare the score of portfolios
def compare_candidate (x,y):
  if evaluate(x,t,A) > evaluate(y,t,A):
    return -1
  else:
    if evaluate(x,t,A) == evaluate(y,t,A):
      return 0
    else:
      return 1

# survival selection
def survival_and_reproduction(pop):
  pop = pop.sort(compare_candidate)
  pop_size = len(pop)
  half_pop_size = round(pop_size/2)
  def sum_from_1 (n):
    if n == 0:
      return 0
    else:
      s = 0
      for i in range(1,n):
        s = s + i
      return s


# get the stocks number of each portfolio
p_len = int(len(A[0,:]))

# define cross functino to cross portfolio a and b in 
def cross(a, b, A):
  for j in range(0, p_c):
    a_j, b_j = A[a][j], A[b][j]
    mean, sigma = (a_j + b_j) / 2, abs(a_j - b_j) / 2 
    r_j = random.normalvariate(mean, sigma)
    A[a][j] = r_j
    A[b][j] = a_j + b_j - r_j
  print A


#test case, cross portfolio 1 and portfolio 2
cross(0,1, A)

def sample_with_replacement(r, n):
  lst = []
  for i in range(0, n):
    lst.extend(random.sample(xrange(r), 1))
  print lst
  return lst   

#mutate: arrary -> array

# define mutate function to mutate k% values in array
def mutate(k, A):
  n = int(k * p_r * p_c/100)
  print "n", n
  random_row = sample_with_replacement(p_r, n)
  random_col = sample_with_replacement(p_c, n)
  for i in range(n):
    A[random_row[i]][random_col[i]] = 100000
  print A

 #test case with 5% mutation rate  
mutate(20, A)



